<?php
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
  <head>
    <?php print $head ?>
    <title><?php print $head_title ?></title>
    <?php print $styles ?>
    <?php print $scripts ?>
  </head>
<body class="<?php print $body_classes ?>">
<div id="templatemo_wrapper">
	<div id="templatemo_header">
    
        <div id="site_title">
			<?php if($logo): ?>			
			<div id="logo"><a href="<?php print base_path() ?>"><img src="<?php print $logo ?>" /></a></div>
			<?php endif; ?>
			<?php if($site_name): ?>			
			<div id="site-name"><a href="<?php print base_path() ?>"><?php print $site_name ?></a></div>
			<?php endif; ?>
        </div> <!-- end of site_title -->
        
        <div id="templatemo_menu">
            <?php print theme('links', $primary_links, array('class' => 'links primary-links')); ?>
	    </div> <!-- end of templatemo_menu -->
    
    </div> <!-- end of header -->
    
    <div id="templatemo_main">
    	
        <div id="templatemo_content">
            <div class="content_box">
				<?php if($welcome): ?><div class="welcome">
<?php print $welcome ?></div>
<?php endif; ?>
				<?php if($tabs || $tabs2): ?>    				
					<div class="tabs">
							<?php print $tabs ?>
							<?php print $tabs2 ?>
						</div>
					<?php endif; ?>
						<?php print $content ?>
                <div class="cleaner"></div>
        	</div>
        </div> <!-- end of content -->
        
        <div id="templatemo_sidebar">
        
        	<div class="sidebar_box_woframe">
        	         <?php print theme('links', $secondary_links, array('class' => 'links secondary-links')); ?>
                <div class="cleaner"></div>
					</div>
              <?php print $rightside ?>
              <div class="cleaner"></div>
                
        </div> <!-- end of sidebar -->
    
    	<div class="cleaner"></div>
    </div> <!-- end of main -->
    
     <div id="templatemo_footer">
			<div style="margin-bottom:5px"><?php print $footer_message ?></div>
			<?php print $footer ?>
    
    </div> <!-- end of templatemo_footer -->
    
</div> <!-- end of warpper -->

</body>
</html>
